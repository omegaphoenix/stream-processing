# **Stream** Processing

---

@title[Add A Little Imagination]

@snap[north-west span-50 text-center]
### Last Time
@snap[west span-55]
@ul[list-spaced-bullets text-09]
- Batch Processing
- Unix Philosophy
- MapReduce
- Glide
- Data Delivery (Old)
@ulend
@snapend

@snap[south span-100 bg-black fragment]
@img[shadow](assets/img/glide.png)
@snapend


---

### Stream Processing at 2S
@ul[list-spaced-bullets text-09]
- Silver (DRIP)
- RAE (Workflow)
- Logs and Metrics
- RabbitMQ
- Pulsar
@ulend

---

### Transmitting Event Streams

---

### Events
@ul[list-spaced-bullets text-09]
- in DRIP
- in Workflow
@ulend

---

### Multiple Consumers
@ul[list-spaced-bullets text-09]
- Postgres triggers
- RabbitMQ fanout
- Rust (tokio) stream fanout
@ulend

---

### Messaging Systems - Producers outspeeding consumers
@ul[list-spaced-bullets text-09]
- Backpressure
  - Rust channels in workflow
- Buffer
  - Rabbit queue in DRIP
  - Performance
- Drop Messages
  - triage, time based message skipping
@ulend

---

### Messaging Systems - Durability
@ul[list-spaced-bullets text-09]
- DRIP uses RabbitMQ
- Redstone keeps jobs in the database
@ulend

---

### Messaging Systems - Direct Messaging vs Message Brokers
@ul[list-spaced-bullets text-09]
- RabbitMQ
- Pulsar
- ZeroMQ
- Roadrunner tracks
@ulend

---

### Partitioned Logs
@ul[list-spaced-bullets text-09]
- Issue with transient messaging
- Circular buffer/ring buffer
  - Metrics timeline
  - Animator state
@ulend

@snap[south span-100 bg-black fragment]
@img[shadow](assets/img/timeline.png)
@snapend


---

### Databases and Streams
@ul[list-spaced-bullets text-09]
- Keeping Systems in Sync
- Change Data Capture
- Event Sourcing
@ulend

---

### State, Streams and Immutability
@ul[list-spaced-bullets text-09]
- Immutability
- Durable changelog = reproducible state
  - DRIP - replay is a first class citizen
  - Workflow
- Event Sourcing
@ulend

---

### Processing Streams

---

### Questions?

---

### Stream Processing

#### Workflow
#### DRIP
#### Transmitting Event Streams
#### Databases and Streams
#### Processing Streams


---
